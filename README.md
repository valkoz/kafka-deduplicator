### На данный момент проект состоит из 4 сервисов:

1. ignite
2. kafka
3. generator - отправляет сообщения в кафку с заданной вероятностью дублей в потоке: 20%, 50%, 80%
4. consumer - подписывается на топики кафки, рассчитывает хэши сообщений и сравнивает их со значениями в ignite. В зависимоти от эксперимента работает с одним кэшем или несколькими разными

### Build and Deploy

##### Cluster

1. Build jar's with gradle
2. Push artifacts via rsync `rsync -a -e "ssh -p 9005" . root@samos.dozen.mephi.ru:/home/deduplicator`
3. Connect via ssh `ssh -p 9005 root@samos.dozen.mephi.ru`
4. Check already running services `docker service ls`
5. Check cluster configuration `docker node ls`
6. Deploy `docker stack deploy --compose-file stack.yml test`
7. Check services up `docker service ls`
8. Show logs `docker service logs -f test_consumer`
9. Shutdown services `docker stack rm test`
10. Prune images `docker system prune -a -f`

##### Local

1. Init cluster `sudo docker swarm init --advertise-addr 127.0.0.1`
2. Deploy `sudo docker stack deploy --compose-file stack.yml test`
3. Check services up `sudo docker service ls`
4. Show logs `sudo docker service logs -f test_consumer`
5. Remove cluster `sudo docker swarm leave --force`

[OLD] ~~Все сервисы запускаются в docker с помощью docker-compose: `sudo docker-compose up --build`~~

Замечания:

1. Ignite кластер поднимается сам (видно по количеству нод в логах)
2. Docker переподнимает generator и consumer через несколько секунд после окончания их раоты