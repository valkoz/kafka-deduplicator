package ru.mephi.dozen;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.*;

public class Generator {

    private final static Map<String, Double> TOPIC_TO_DUPLICATE_RATE = new HashMap<>();

    private final static String KAFKA_LOCAL = "kafka:29092";

    private final static String DUPLICATE_UUID = UUID.randomUUID().toString();

    private final static int TOTAL_MESSAGES_PER_TOPIC_COUNT = 10_000;

    public static void main(String[] args) throws Exception {
        System.out.println("Generator started successfully...");

        ConfigurationProvider configurationProvider = new ConfigurationProvider();
        Configuration configuration = configurationProvider.getDefaultConfiguration();

        for (ConfigurationData data : configuration.getConfig()) {
            TOPIC_TO_DUPLICATE_RATE.put(data.getKafkaTopicName(), data.getDuplicationRate());
        }

        if (args.length == 0) {
            runProducer(TOTAL_MESSAGES_PER_TOPIC_COUNT);
        } else {
            runProducer(Integer.parseInt(args[0]));
        }
    }

    private static Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_LOCAL);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    private static void runProducer(final int sendMessageCount) throws Exception {
        final Producer<Long, String> producer = createProducer();
        long time = System.currentTimeMillis();
        System.out.println("Generator sending messages...");
        try {
            for (long key = time; key < time + sendMessageCount; key++) {
                for (Map.Entry<String, Double> entry : TOPIC_TO_DUPLICATE_RATE.entrySet()) {
                    final ProducerRecord<Long, String> record =
                            new ProducerRecord<>(
                                    entry.getKey(),
                                    key,
                                    getMessage(entry.getValue())
                            );
                    producer.send(record).get();
                }
            }
        } finally {
            producer.flush();
            producer.close();
        }
    }

    private static String getMessage(Double duplicateRate) {
        if (Math.random() > duplicateRate) {
            return UUID.randomUUID().toString();
        } else {
            return DUPLICATE_UUID;
        }
    }
}
