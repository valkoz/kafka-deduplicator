package ru.mephi.dozen;

import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.client.ClientCache;
import org.apache.ignite.client.ClientCacheConfiguration;
import org.apache.ignite.client.IgniteClient;
import org.apache.ignite.configuration.ClientConfiguration;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.groupingBy;

public class Subscriber {

    private final static String KAFKA_LOCAL = "kafka:29092";

    private final static String IGNITE_LOCAL = "ignite-master:10800";

    private static MessageDigest digest;

    private static final Map<String, ClientCache<String, Long>> KAFKA_TOPIC_TO_IGNITE_CACHE = new HashMap<>();

    private static List<Long> measuredExecutionTime = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Subscriber started successfully...");

        ConfigurationProvider configurationProvider = new ConfigurationProvider();
        Configuration configuration = configurationProvider.getDefaultConfiguration();

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            ClientConfiguration cfg = new ClientConfiguration().setAddresses(IGNITE_LOCAL);
            IgniteClient igniteClient = Ignition.startClient(cfg);

            for (ConfigurationData configurationData : configuration.getConfig()) {
                ClientCacheConfiguration cacheConfiguration = new ClientCacheConfiguration();
                cacheConfiguration.setBackups(configurationData.getIgniteBackups());
                cacheConfiguration.setCacheMode(CacheMode.REPLICATED);
                cacheConfiguration.setName(configurationData.getIgniteCacheName());

                KAFKA_TOPIC_TO_IGNITE_CACHE.put(configurationData.getKafkaTopicName(), igniteClient.getOrCreateCache(cacheConfiguration));
            }

            runConsumer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Consumer<Long, String> createConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_LOCAL);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        final Consumer<Long, String> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(KAFKA_TOPIC_TO_IGNITE_CACHE.keySet());
        return consumer;
    }

    private static void runConsumer() {
        final Consumer<Long, String> consumer = createConsumer();

        final int giveUp = 10;
        int noRecordsCount = 0;

        while (true) {
            final ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));

            if (consumerRecords.count() == 0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                String hexHash = bytesToHex(Subscriber.digest.digest(record.value().getBytes()));
                filterDuplicates(hexHash, record);
            });

            consumer.commitAsync();
        }
        consumer.close();

        AtomicInteger counter = new AtomicInteger();

        measuredExecutionTime
                .stream()
                .collect(groupingBy(x-> counter.getAndIncrement() / 1000))
                .values()
                .forEach(it -> System.out.println(it.stream().mapToDouble(Long::doubleValue).average()));

        System.out.println("Results: " + String.valueOf(measuredExecutionTime.stream().mapToDouble(Long::doubleValue).average()));
    }

    private static void filterDuplicates(String hexHash, ConsumerRecord record) {

        ClientCache<String, Long> clientCache = KAFKA_TOPIC_TO_IGNITE_CACHE.get(record.topic());

        long time = System.nanoTime();

        if (!clientCache.containsKey(hexHash)) {
            clientCache.put(hexHash, time);
        }
        long elapsed = System.nanoTime() - time;
        measuredExecutionTime.add(elapsed);
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
