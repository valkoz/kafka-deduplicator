Для того, чтобы кластер заработал надо проделать следующие шаги:

* Установить докер на каждую машину:

```
sudo yum install -y yum-utils   device-mapper-persistent-data   lvm2
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce
docker --version
sudo systemctl enable docker.service
sudo systemctl start docker.service
```

* Проинициализировать кластер на мастер ноде (и открыть порты):

```
docker swarm init
systemctl status firewalld
systemctl start firewalld
systemctl enable firewalld
firewall-cmd --add-port=2376/tcp --permanent
firewall-cmd --add-port=2377/tcp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=4789/udp --permanent
firewall-cmd --reload
systemctl restart docker
docker node ls
```

* Подключить слейвов к мастеру (предварительно открыть порты):

```
systemctl status firewalld
systemctl start firewalld
systemctl enable firewalld
firewall-cmd --add-port=2376/tcp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=4789/udp --permanent
firewall-cmd --reload
systemctl restart docker
docker swarm join --token <token> 192.168.12.65:2377
```

* Проверить на мастере все ли узлы подклчюлись:

```
docker node ls
```

Output:

```
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
r4lyu8c4kdmj5nnm3x6393nb3 *   var1-vm5            Ready               Active              Leader              19.03.4
2s27njtyelkxql6senj3rb6nm     var2-vm5            Ready               Active                                  19.03.4
j6ncsongc0l3nc1mj47408whq     var2-vm6            Ready               Active                                  19.03.4

```

Links:

[Установка Docker в CentOS](https://github.com/NaturalHistoryMuseum/scratchpads2/wiki/Install-Docker-and-Docker-Compose-(Centos-7))

[Docker Swarm official docs](https://docs.docker.com/engine/swarm/swarm-tutorial/)

[Docker Swarm Firewall](https://www.digitalocean.com/community/tutorials/how-to-configure-the-linux-firewall-for-docker-swarm-on-centos-7)
