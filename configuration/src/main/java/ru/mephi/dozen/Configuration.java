package ru.mephi.dozen;

import java.util.List;

public class Configuration {
    private List<ConfigurationData> config;

    public List<ConfigurationData> getConfig() {
        return config;
    }

    public void setConfig(List<ConfigurationData> config) {
        this.config = config;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "config=" + config +
                '}';
    }
}
