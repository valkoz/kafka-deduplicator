package ru.mephi.dozen;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.*;

public class ConfigurationProvider {

    public Configuration getConfiguration(String path) {
        Constructor constructor = new Constructor(Configuration.class);
        Yaml yaml = new Yaml(constructor);

        try(InputStream input = new FileInputStream(new File(path))) {
            Configuration configuration = yaml.loadAs(input, Configuration.class);
            System.out.println(configuration);
            return configuration;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Configuration getDefaultConfiguration() {
        return getConfiguration("config.yml");
    }
}
