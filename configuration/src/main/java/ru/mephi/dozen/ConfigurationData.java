package ru.mephi.dozen;

public class ConfigurationData {
    private String kafkaTopicName;
    private String igniteCacheName;
    private Double duplicationRate;
    private Integer igniteBackups;

    public String getKafkaTopicName() {
        return kafkaTopicName;
    }

    public void setKafkaTopicName(String kafkaTopicName) {
        this.kafkaTopicName = kafkaTopicName;
    }

    public String getIgniteCacheName() {
        return igniteCacheName;
    }

    public void setIgniteCacheName(String igniteCacheName) {
        this.igniteCacheName = igniteCacheName;
    }

    public Double getDuplicationRate() {
        return duplicationRate;
    }

    public void setDuplicationRate(Double duplicationRate) {
        this.duplicationRate = duplicationRate;
    }

    public Integer getIgniteBackups() {
        return igniteBackups;
    }

    public void setIgniteBackups(Integer igniteBackups) {
        this.igniteBackups = igniteBackups;
    }

    @Override
    public String toString() {
        return "ConfigurationData{" +
                "kafkaTopicName='" + kafkaTopicName + '\'' +
                ", igniteCacheName='" + igniteCacheName + '\'' +
                ", duplicationRate=" + duplicationRate +
                ", igniteBackups=" + igniteBackups +
                '}';
    }
}
